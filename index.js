
module.exports = { createLogger };


let logLevels = ['trace', 'debug', 'info', 'warning', 'error', 'critical'];
let logLevelLabels = ['trce', 'dbug', 'info', 'warn', 'fail', 'crit'];
let logLevelColors = [
    '\x1B[37m\x1B[40m',
    '\x1B[37m\x1B[40m',
    '\x1B[32m\x1B[40m',
    '\x1B[1m\x1B[33m\x1B[40m',
    '\x1B[30m\x1B[41m',
    '\x1B[1m\x1B[37m\x1B[41m'
];

let defaultConsoleColor = '\x1B[39m\x1B[22m\x1B[49m';

let logLevelThreshold = parseInt(process.env['LOG_LEVEL_THRESHOLD'] || '', 10) || 0;
let disableColors = (process.env['LOG_DISABLE_COLORS'] || '').toLowerCase() === 'true';


function createLogger(category) {

    let loggers = {};

    for (let i = 0; i < logLevels.length; i++) {
        loggers[logLevels[i]] = i >= logLevelThreshold
            ? logMessage.bind({ disableColors }, i, category)
            : () => { };
    }

    return loggers;
}

function logMessage(level, category, message, error, eventId) {
    
    let formattedTimeStamp = formatLocalTime(new Date());
    let formattedMessage = `${message || ''}`.split('\n').map(line => `      ${line}`).join('\n');
    let formattedError = error ? `${error}\n` : '';
    let formattedLevel = this && this.disableColors
        ? `${logLevelLabels[level] || ''}`
        : `${logLevelColors[level] || defaultConsoleColor}${logLevelLabels[level] || ''}${defaultConsoleColor}`;
    
    process.stdout.write(`\n${formattedTimeStamp} ${formattedLevel}: ${category || ''}[${eventId || 0}]\n${formattedMessage}\n${formattedError}`);
}

function formatLocalTime(t) {

    let pad2 = s => ('0' + s).slice(-2);
    let pad3 = s => ('00' + s).slice(-3); 

    let year = t.getFullYear();
    let month = pad2(t.getMonth() + 1);
    let day = pad2(t.getDate());
    let hours = pad2(t.getHours());
    let minutes = pad2(t.getMinutes());
    let seconds = pad2(t.getSeconds());
    let milliseconds = pad3(t.getMilliseconds());

    return `${year}-${month}-${day} ${hours}:${minutes}:${seconds}.${milliseconds}`;
}

