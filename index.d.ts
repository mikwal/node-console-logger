export type LogLevel = 'trace' | 'debug' | 'info' | 'warning' | 'error' | 'critical';

export type Logger = { [level in LogLevel]: (message: string, error?: Error, eventId?: number) => void; };

export declare function createLogger(category: string): Logger;