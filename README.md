# NodeJS console logger

Standard output logger for NodeJS micro services matching .NET Core style output format using the time stamp format below:
```CSharp 
TimeStampFormat = "\nyyyy-MM-dd HH\\:mm\\:ss.fff "
```

Designed with Amazon ECS in mind.  